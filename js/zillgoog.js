(function($, Drupal, drupalSettings) {
    Drupal.behaviors.zillow = {
        attach: function(context, settings) {
            /**
             * Namespace for the zillgoog app
             */
            var zillgoog = {};

            function updateURLParameter(url, param, paramVal)
            {
                var TheAnchor = null;
                var newAdditionalURL = "";
                var tempArray = url.split("?");
                var baseURL = tempArray[0];
                var additionalURL = tempArray[1];
                var temp = "";

                if (additionalURL)
                {
                    var tmpAnchor = additionalURL.split("#");
                    var TheParams = tmpAnchor[0];
                    TheAnchor = tmpAnchor[1];
                    if(TheAnchor)
                        additionalURL = TheParams;

                    tempArray = additionalURL.split("&");

                    for (i=0; i<tempArray.length; i++)
                    {
                        if(tempArray[i].split('=')[0] != param)
                        {
                            newAdditionalURL += temp + tempArray[i];
                            temp = "&";
                        }
                    }
                }
                else
                {
                    var tmpAnchor = baseURL.split("#");
                    var TheParams = tmpAnchor[0];
                    TheAnchor  = tmpAnchor[1];

                    if(TheParams)
                        baseURL = TheParams;
                }

                if(TheAnchor)
                    paramVal += "#" + TheAnchor;

                var rows_txt = temp + "" + param + "=" + paramVal;
                return baseURL + "?" + newAdditionalURL + rows_txt;
            }

            /**
             * Constructor for zillgoog.map
             * @constructor
             * @param primary_property - property object
             * @param options - options bag
             */
            zillgoog.map = function( primary_property, options ) {
                this.primaryProperty = primary_property;

                if( !options ) options = {};

                var map_id = options.canvas || 'map_canvas';
                var initial_zoom_level = options.zoomLevel || 8;
                var lat_lon = new google.maps.LatLng(this.primaryProperty.x, this.primaryProperty.y);

                this.pano_ = null;
                this.panoService_ = new google.maps.StreetViewService();
                this.map_ = new google.maps.Map(document.getElementById(map_id), {
                    zoom: initial_zoom_level,
                    center: lat_lon,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });
                this.geocoder = new google.maps.Geocoder;
                this.marker_geo = new google.maps.Marker({
                    map: this.map_
                });
                this.letterPosition = 0;
                this.lastClickedStreetViewProperty = null;

                this.bounds_ = new google.maps.LatLngBounds();
                this.createMarker( this.primaryProperty );

                this.infoWindow = new google.maps.InfoWindow({
                    maxWidth: 350,
                    content: '<div id="infoWindowContent" style="width:350px;height:200px;"></div>'
                });
                this.infowindow_geo = new google.maps.InfoWindow();

                this.pin_ = new google.maps.MVCObject();

                var zillGoogMap = this;
                google.maps.event.addListener(this.infoWindow, 'domready', function() {
                    if (zillGoogMap.pano_ != null) {
                        zillGoogMap.pano_.unbind("position");
                        zillGoogMap.pano_.setVisible(false);

                    }

                    zillGoogMap.pano_ = new google.maps.StreetViewPanorama(document.getElementById("infoWindowContent"), {
                        navigationControl: false,
                        enableCloseButton: false,
                        addressControl: false,
                        linksControl: false,
                        visible: true
                    });
                    zillGoogMap.pano_.bindTo("position", zillGoogMap.pin_);
                    zillGoogMap.pano_.setVisible(true);

                    var loc = new google.maps.LatLng(zillGoogMap.pin_.position.lat(), zillGoogMap.pin_.position.lng());
                    zillGoogMap.panoService_.getPanoramaByLocation(loc, 50, function(result, status) {
                        var camera = result.location.latLng;
                        // We have to know which direction to point the camera, so we calculate the
                        // bearing between the address position and the camera position.
                        var bearing = LatLon.bearing( camera.lat(), camera.lng(), loc.lat(), loc.lng() );
                        var pov = {heading: bearing, pitch:0, zoom:1};
                        zillGoogMap.pano_.setPov(pov)
                    });
                });

                google.maps.event.addListener(this.infoWindow, 'closeclick', function() {
                    zillGoogMap.pano_.unbind("position");
                    zillGoogMap.pano_.setVisible(false);
                    zillGoogMap.pano_ = null;
                });

                //Listen for any clicks on the map.
                google.maps.event.addListener(this.map_, 'click', function(event) {
                    //Get the location that the user clicked.
                    var clickedLocation = event.latLng;
                    zillGoogMap.geocoder.geocode({'location': clickedLocation}, function(results, status) {
                        if (status === google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                var newURL = $(location).attr('origin') + '/' + $(location).attr('pathname').split('/')[1];
                                var street_number = false;
                                var route = false;
                                var zip = false;
                                for (var j = 0; j < results[0].address_components.length; j++) {
                                    switch (results[0].address_components[j].types[0]) {
                                        case 'street_number':
                                            street_number = results[0].address_components[j].short_name;
                                            break;
                                        case 'route':
                                            route = results[0].address_components[j].long_name;
                                            break;
                                        case 'postal_code':
                                            zip = results[0].address_components[j].short_name;
                                            break;
                                    }
                                }
                                zillGoogMap.marker_geo.setPosition(clickedLocation);
                                if (street_number && route && zip) {
                                    newURL += '/' + street_number + ' ' + encodeURI(route) + '/' + zip;
                                    zillGoogMap.infowindow_geo.setContent('<b><h4>Search address:</h4></b><a href="' + newURL + '">' + results[0].formatted_address + '</a>');
                                } else {
                                    zillGoogMap.infowindow_geo.setContent('<b><h4>Address:</h4></b>' + results[0].formatted_address);
                                }
                                zillGoogMap.infowindow_geo.open(zillGoogMap.map_, zillGoogMap.marker_geo);
                            }
                        }
                    });
                });
            };

            /**
             * Creates a marker and set's it letter to the next in the list
             * @param property - a property object
             * @return {google.maps.Marker}
             */
            zillgoog.map.prototype.createMarker = function( property )
            {
                var zillGoogMap = this;

                var letters = zillgoog.map.letters;

                var latlong = new google.maps.LatLng(property.x, property.y);

                if (this.letterPosition === 0) {
                    var icon_url = "http://mt.google.com/vt/icon/name=icons/spotlight/home_L_8x.png&scale=1.5";
                } else {
                    var icon_url = "http://mt.google.com/vt/icon?psize=16&font=fonts/Roboto-Regular.ttf&color=ff330000&name=icons/spotlight/spotlight-waypoint-a.png&ax=44&ay=48&scale=1&text=" + letters[this.letterPosition-1];
                }

                var marker = new google.maps.Marker({
                    position: latlong,
                    map: this.map_,
                    icon: new google.maps.MarkerImage(icon_url),
                    animation: google.maps.Animation.DROP
                });

                google.maps.event.addListener(marker, 'click', function() {
                    zillGoogMap.openInfoWindow(marker, zillGoogMap);
                });

                this.bounds_.extend(latlong);
                this.map_.fitBounds(this.bounds_);

                this.letterPosition = this.letterPosition + 1;

                return marker;
            };

            zillgoog.map.prototype.openInfoWindow = function( marker, context ) {
                context.pin_.set("position", marker.getPosition());
                context.infoWindow.open(context.map_, marker);
            };


            /**
             * Adds comparable addresses to the map. When all of them have been set,
             * the map zoom level is adjusted to encompass all of the addresses (including
             * the primary property)
             * @param [properties] - array of properties to add
             * @return void
             */
            zillgoog.map.prototype.addComparables = function( properties )
            {
                for( var i = 0; i < properties.length; i++ )
                {
                    this.createMarker( properties[i] );
                }
            };

            zillgoog.map.letters = [
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
                'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                'Y', 'Z'
            ];

            $(document).ready(function(){
                var pm    = settings.zillow.resultsPage.primary_placemark;
                var comps = settings.zillow.resultsPage.comp_placemarks;
                    //map_initialize( pm );
                    var zg = new zillgoog.map(pm);
                    zg.addComparables( comps );

                    //Apply a row click effect
                    $('#comps table tbody tr').each(function(){
                    var $anchor = $('a', this);
                    var anchor_text = $anchor.text();
                    var href    = $anchor.attr('href');

                    $anchor.after(anchor_text);
                    $anchor.remove();

                    $(this).click(function(){
                        window.location = href;
                        return false;
                    });
                });

            });

        }
    };
})(jQuery, Drupal, drupalSettings);
