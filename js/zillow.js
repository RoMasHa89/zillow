(function($) {
    $( document ).ready(function() {
        $("input.geocomplete").geocomplete();
        $("input.citycomplete").geocomplete();
        $.fn.search_alert = function(data) {
            $('input').filter(function() {
                if (this.value == data) {
                    this.setCustomValidity('error');
                    $(this).parent().next().hide().css({ visibility: 'hidden' }).slideDown("normal", function() {
                        $(this).css('visibility', 'visible');
                    });
                    $(this).on('focus', function() {
                        $(this).parent().next().hide();
                    });
                }
            });
        };
    });
} (jQuery));