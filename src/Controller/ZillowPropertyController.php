<?php
/**
 * @file
 * Contains \Drupal\zillow\Controller\ZillowPropertyController.
 */

namespace Drupal\zillow\Controller;

use Drupal\Core\Controller\ControllerBase;

use Drupal\zillow\Zillow\ZillowClient;
use Drupal\zillow\Zillow\ZillowException;

use Drupal\zillow\GoogleGeocoder;

/**
 * Class ZillowPropertyController.
 *
 * @package Drupal\zillow\Controller
 */
class ZillowPropertyController extends ControllerBase {

  /**
   * Flatten array to one-dimensional, if input array is multi-dimensional.
   *
   * @param array $array
   *    Input array.
   * @param bool $current_key
   *    Store "path" key.
   *
   * @return array
   *    Flattened array.
   */
  public function optionsArrayFlatten($array, $current_key = FALSE) {
    $result = array();
    if (is_array($array)) {
      foreach ($array as $key => $value) {
        if (is_array($value)) {
          $result += self::options_array_flatten($value, "{$current_key}/{$key}");
        }
        else {
          $result["{$current_key}/{$key}"] = $value;
        }
      }
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function zillowPropertyResultPage($street, $zip) {

    // Initialize data arrays.
    $property = $chart_url = $comps = $primary_placemark = $comp_placemarks = array();

    $options_chart = array(
      '1year' => '1',
      '5years' => '5',
      '10years' => '10',
    );

    // Get API Key from zillow module settings.
    $key = \Drupal::config('zillow.settings')->get('api_key');
    // Get chart period.
    $chart_period = \Drupal::config('zillow.settings')->get('chart_period') ?: '1year';
    // Get comparables count.
    $comps_num = \Drupal::config('zillow.settings')->get('comps_num') ?: 5;

    // Initialize Geocoder.
    $gc = new GoogleGeocoder();

    // Initialize Zillow API client.
    $client = new ZillowClient($key);

    // Send GetDeepSearchResults request.
    try {
      $client->GetDeepSearchResults(
        [
          'address' => $street,
          'citystatezip' => $zip,
          'rentzestimate' => TRUE,
        ]
      );
    }
    catch (ZillowException $e) {
      // Catch an exception if needed.
    }

    // If response successful - get received data...
    if ($client->isSuccessful() && !isset($e)) {

      $response = $client->getResults();

      if (!empty($response)) {
        $property = array_pop($response);
        if (!empty($property['zpid'])) {
          // Get Google placemark.
          $primary_placemark = $gc->geocode($property['address']['street'] . ' ' . $property['address']['city'] . ' ' . $property['address']['state'] . ' ' . $property['address']['zipcode']);

          // Send GetChart request.
          try {
            $client->GetChart(
              [
                'zpid' => $property['zpid'],
                'unit-type' => 'dollar',
                'width' => 500,
                'height' => 275,
                'chartDuration' => $chart_period,
              ]
            );
          }
          catch (ZillowException $e) {
            // Catch an exception if needed.
          }
          // If response successful - get received data..
          if ($client->isSuccessful()) {
            $response = $client->getResponse();
            if (!empty($response['url'])) {
              // Store chart URL.
              $chart_url = $response['url'];
            }
          }

          // Send GetDeepComps request.
          try {
            $client->GetDeepComps(
              [
                'zpid' => $property['zpid'],
                'count' => $comps_num,
                'rentzestimate' => TRUE,
              ]
            );
          }
          catch (ZillowException $e) {
            // Catch an exception if needed.
          }
          // If response successful - get received data..
          if ($client->isSuccessful()) {
            $response = $client->getResponse();
            if (!empty($response['properties']['comparables']['comp'])) {
              // Check if we got single result.
              if (count(array_filter(array_keys($response['properties']['comparables']['comp']), 'is_string')) > 0) {
                $comps[] = $response['properties']['comparables']['comp'];
              }
              else {
                $comps = $response['properties']['comparables']['comp'];
              }
            }
            // If any results - get address placemarks.
            if (!empty($comps)) {
              foreach ($comps as $comp) {
                $comp_placemarks[] = $gc->geocode($comp['address']['street'] . ' ' . $comp['address']['city'] . ' ' . $comp['address']['state'] . ' ' . $comp['address']['zipcode']);
              }
            }
          }
        }
      }
      // Construct element and it's data.
      $element = array(
        '#theme' => 'zillow_property_result_page',
        '#message' => $this->t('<h1>Zillow API Results for <b>"@address"</b>:</h1>', array('@address' => $property['address']['street'])),
        '#property' => $property,
        '#chart_url' => $chart_url,
        '#chart_period' => $options_chart[$chart_period],
        '#comps' => $comps,
        '#error' => FALSE,
      );

      // Add the library.
      $element['#attached']['library'][] = 'zillow/results.page';
      $element['#attached']['drupalSettings']['zillow']['resultsPage']['primary_placemark'] = $primary_placemark;
      $element['#attached']['drupalSettings']['zillow']['resultsPage']['comp_placemarks'] = $comp_placemarks;

      return $element;

    }
    else {
      if (isset($e)) {
        // If got exception - get message.
        $message = $e->getMessage();
      }
      else {
        // Get status code and message.
        $message = $client->getStatusCode() . ':' . $client->getStatusMessage();
      }

      // Construct element and it's data.
      $element = array(
        '#theme' => 'zillow_property_result_page',
        '#message' => $this->t('<h1>No Property Results found!</h1>@message', array('@message' => $message)),
        '#error' => TRUE,
      );

      return $element;
    }

  }

}
