<?php
/**
 * @file
 * Contains \Drupal\zillow\Controller\ZillowRegionController.
 */

namespace Drupal\zillow\Controller;

use Drupal\Component\Utility\SafeMarkup;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\zillow\Zillow\ZillowClient;
use Drupal\zillow\Zillow\ZillowException;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ZillowRegionController.
 *
 * @package Drupal\zillow\Controller
 */
class ZillowRegionController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function zillowRegionResultPage($state, $childtype, $city) {

    // Set search string.
    $search = $state . ($city !== '#' ? ', ' . $city : '');

    // Get Zillow API Key from zillow module settings.
    $key = \Drupal::config('zillow.settings')->get('api_key');
    // Get Google API Key from zillow module settings.
    $google_key = \Drupal::config('zillow.settings')->get('google_api_key');

    // Initialize Zillow API client.
    $client = new ZillowClient($key);

    // Send GetDeepSearchResults request.
    try {
      $client->GetRegionChildren(
        [
          'state' => $state,
          'city' => $city !== '#' ? $city : '',
          'childtype' => $childtype,
        ]
      );
    }
    catch (ZillowException $e) {
      // Catch an exception if needed.
    }

    // If response successful - get received data...
    if ($client->isSuccessful() && !isset($e)) {

      $table = array();
      $response = $client->getResponse();

      if (!empty($response) && $response['list']['count'] > 0) {
        if (is_array(array_pop($response['list']['region']))) {
          foreach ($response['list']['region'] as $index => $element) {
            if (!empty($element['name'])) {
              $table[$index][] = $index + 1;
              if (!empty($element['url'])) {
                $table[$index][] = Link::fromTextAndUrl(
                  $element['name'],
                  Url::fromUri($element['url']),
                  array('external' => TRUE)
                )->toString();
              }
              elseif ($childtype === 'zipcode') {
                $table[$index][] = Link::fromTextAndUrl(
                  $element['name'],
                  Url::fromUri("http://www.zillow.com/homes/{$element['name']}_rb/"),
                  array('external' => TRUE)
                )->toString();
              }
              else {
                $table[$index][] = $element['name'];
              }
              $table[$index][] = $element['id'];
              $table[$index][] = isset($element['zindex']['@currency']) ? number_format((double) $element['zindex']['#'], 2) . ' ' . $element['zindex']['@currency'] : '';
              $search_query = Request::normalizeQueryString($state . ' ' . $element['name']);
              $table[$index][] = SafeMarkup::format('<img src="http://maps.googleapis.com/maps/api/staticmap?center=@url&scale=false&size=128x128&maptype=roadmap&format=png&visual_refresh=true&key=@key" alt="@map_msg">', array(
                '@url' => $search_query,
                '@key' => $google_key,
                '@map_msg' => empty($google_key) ? t('No API key') : t('No image'),
              ));
            }
          }
        }
        else {
          $table[0][] = 1;
          if (!empty($response['list']['region']['url'])) {
            $table[0][] = Link::fromTextAndUrl(
              $response['list']['region']['name'],
              Url::fromUri($response['list']['region']['url']),
              array('external' => TRUE)
            )->toString();
          }
          elseif ($childtype === 'zipcode') {
            $table[0][] = Link::fromTextAndUrl(
              $response['list']['region']['name'],
              Url::fromUri("http://www.zillow.com/homes/{$response['list']['region']['name']}_rb/"),
              array('external' => TRUE)
            )->toString();
          }
          else {
            $table[0][] = $response['list']['region']['name'];
          }
          $table[0][] = $response['list']['region']['id'];
          $table[0][] = isset($response['list']['region']['zindex']['@currency']) ? number_format((double) $response['list']['region']['zindex']['#'], 2) . ' ' . $response['list']['region']['zindex']['@currency'] : '';
          $search_query = Request::normalizeQueryString($state . ' ' . $response['list']['region']['name']);
          $table[0][] = SafeMarkup::format('<img src="http://maps.googleapis.com/maps/api/staticmap?center=@url&scale=false&size=128x128&maptype=roadmap&format=png&visual_refresh=true&key=@key" alt="@map_msg">', array(
            '@url' => $search_query,
            '@key' => $google_key,
            '@map_msg' => empty($google_key) ? t('No API key') : t('No image'),
          ));
        }
        // Number of records shown in per page.
        $per_page = \Drupal::config('zillow.settings')->get('region_results_limit') ?: 50;
        $current_page = pager_default_initialize(count($table), $per_page);
        // Divide results by page limitation.
        $chunks = array_chunk($table, $per_page, TRUE);

        // Construct element and it's data.
        $element = array(
          '#theme' => 'zillow_region_result_page',
          '#message' => $this->t('<h1>Zillow API Results (@count) for <b>"@address"</b>:</h1>', array('@address' => $search, '@count' => $response['list']['count'])),
          '#table' => array(
            '#theme' => 'table',
            '#header' => array('#', t('@label title', array('@label' => ucfirst($childtype))), t('Region ID'), t('Zillow Home Value Index (Zindex®)'), t('Map')),
            '#rows' => $chunks[$current_page],
            '#attributes' => array(),
            '#sticky' => TRUE,
            '#colgroups' => array(),
            '#empty' => t('No Records has been found'),
          ),
          '#pager' => array(
            '#type' => 'pager',
            '#quantity' => count($table),
          ),
          '#error' => FALSE,
        );
        // Add the library.
        $element['#attached']['library'][] = 'zillow/results.page';

        return $element;
      }
      else {
        if (isset($e)) {
          // If got exception - get message.
          $message = $e->getMessage();
        }
        else {
          // Get status code and message.
          $message = $client->getStatusCode() . ':' . $client->getStatusMessage();
        }
        // Construct element and it's data.
        $element = array(
          '#theme' => 'zillow_region_result_page',
          '#message' => $this->t('<h1>No Region Results found!</h1>@message', array('@message' => $message)),
          '#error' => TRUE,
        );

        return $element;
      }
    }
    else {
      if (isset($e)) {
        // If got exception - get message.
        $message = $e->getMessage();
      }
      else {
        // Get status code and message.
        $message = $client->getStatusCode() . ':' . $client->getStatusMessage();
      }

      // Construct element and it's data.
      $element = array(
        '#theme' => 'zillow_region_result_page',
        '#message' => $this->t('<h1>No Region Results found!</h1>@message', array('@message' => $message)),
        '#error' => TRUE,
      );

      return $element;
    }

  }

}
