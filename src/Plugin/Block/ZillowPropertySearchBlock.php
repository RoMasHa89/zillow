<?php
/**
 * @file
 * Contains \Drupal\zillow\Plugin\Block\ZillowPropertySearchBlock.
 */
namespace Drupal\zillow\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormInterface;

/**
 * Provides Zillow Property Search block.
 *
 * @Block(
 *   id = "zillow_property_search_block",
 *   admin_label = @Translation("Zillow Property Search"),
 *   category = @Translation("Blocks")
 * )
 */
class ZillowPropertySearchBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Get Block form.
    $builtForm = \Drupal::formBuilder()->getForm('Drupal\zillow\Plugin\Form\ZillowPropertySearchBlockForm');
    $render['form'] = $builtForm;

    return $render;
  }

}
