<?php
/**
 * @file
 * Contains \Drupal\zillow\Plugin\Block\ZillowRegionSearchBlock.
 */
namespace Drupal\zillow\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormInterface;

/**
 * Provides Zillow Region Search block.
 *
 * @Block(
 *   id = "zillow_region_search_block",
 *   admin_label = @Translation("Zillow Region Search"),
 *   category = @Translation("Blocks")
 * )
 */
class ZillowRegionSearchBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Get Block form.
    $builtForm = \Drupal::formBuilder()->getForm('Drupal\zillow\Plugin\Form\ZillowRegionSearchBlockForm');
    $render['form'] = $builtForm;

    return $render;
  }

}
