<?php
/**
 * @file
 * Contains \Drupal\zillow\Plugin\Form\ZillowPropertySearchBlockForm.
 */
namespace Drupal\zillow\Plugin\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use \Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\AjaxResponse;

use Drupal\zillow\GoogleGeocoder;

/**
 * Implements an ZillowPropertySearchBlockForm.
 */
class ZillowPropertySearchBlockForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'zillow_property_search_block_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['#attached']['library'] = array('zillow/jquery.geocomplete');

    $form['search'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Enter Address:'),
      '#description' => t('Specify property location (Carries validity check)'),
      '#attributes' => array(
        'class' => array('geocomplete'), /* Set class for assigning to input geocoder autocomplete feature. */
      ),
    );
    $form['e_message'] = array(
      '#type' => 'markup',
      '#markup' => '<div class="tooltip">' . $this->t('Wrong address!') . '</div>',
    );
    $form['button'] = array(
      '#type' => 'button',
      '#value' => t('Search'),
      '#ajax' => array(
        'callback' => 'Drupal\zillow\Plugin\Form\ZillowPropertySearchBlockForm::validateAddressAjax',
        'event' => 'click',
        'progress' => array(
          'type' => 'throbber',
          'message' => $this->t('Checking address...'),
        ),
      ),
    );
    return $form;
  }

  /**
   * Address validation callback function.
   *
   * @param array $form
   *    Form.
   * @param FormStateInterface $form_state
   *    FormState.
   *
   * @return AjaxResponse
   *    AjaxResponse.
   */
  public function validateAddressAjax(array &$form, FormStateInterface $form_state) {
    // Instantiate an AjaxResponse Object to return.
    $ajax_response = new AjaxResponse();

    // Get input value.
    $search = $form_state->getValue('search');

    // Initialize Geocoder.
    $gc = new GoogleGeocoder();
    $placemark = $gc->geocode($search);

    // Check for a valid Placemark and act accordingly.
    if ($placemark->isValid() === FALSE) {
      // Add a command, InvokeCommand, which allows for custom jQuery commands.
      $ajax_response->addCommand(new InvokeCommand(NULL, 'search_alert', array($search)));
    }
    else {
      // If address is correct - add data to form. Also check if address is full.
      if (!empty($placemark->street) && !empty($placemark->zip)) {
        $url = Url::fromRoute('zillow.property', array(
          'street' => Request::normalizeQueryString($placemark->streetNumber . ' ' . $placemark->street),
          'zip' => $placemark->zip,
        ), array('absolute' => TRUE));
        // Add a command, InvokeCommand, which allows for custom jQuery commands.
        // On validate - redirect to zillow result page using GET request.
        $ajax_response->addCommand(new RedirectCommand($url->toString()));
      }
      else {
        // Add a command, InvokeCommand, which allows for custom jQuery commands.
        $ajax_response->addCommand(new InvokeCommand(NULL, 'search_alert', array($search)));

      }
    }

    // Return the AjaxResponse Object.
    return $ajax_response;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
