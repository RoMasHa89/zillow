<?php
/**
 * @file
 * Contains \Drupal\zillow\Plugin\Form\ZillowRegionSearchBlockForm.
 */
namespace Drupal\zillow\Plugin\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Locale\CountryManager;
use Drupal\Core\Url;
use \Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\AjaxResponse;

use Drupal\zillow\GoogleGeocoder;

/**
 * Implements an ZillowRegionSearchBlockForm.
 */
class ZillowRegionSearchBlockForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'zillow_region_search_block_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $us_states = array(
      'AL' => 'Alabama',
      'AK' => 'Alaska',
      'AZ' => 'Arizona',
      'AR' => 'Arkansas',
      'CA' => 'California',
      'CO' => 'Colorado',
      'CT' => 'Connecticut',
      'DE' => 'Delaware',
      'FL' => 'Florida',
      'GA' => 'Georgia',
      'HI' => 'Hawaii',
      'ID' => 'Idaho',
      'IL' => 'Illinois',
      'IN' => 'Indiana',
      'IA' => 'Iowa',
      'KS' => 'Kansas',
      'KY' => 'Kentucky',
      'LA' => 'Louisiana',
      'ME' => 'Maine',
      'MD' => 'Maryland',
      'MA' => 'Massachusetts',
      'MI' => 'Michigan',
      'MN' => 'Minnesota',
      'MS' => 'Mississippi',
      'MO' => 'Missouri',
      'MT' => 'Montana',
      'NE' => 'Nebraska',
      'NV' => 'Nevada',
      'NH' => 'New Hampshire',
      'NJ' => 'New Jersey',
      'NM' => 'New Mexico',
      'NY' => 'New York',
      'NC' => 'North Carolina',
      'ND' => 'North Dakota',
      'OH' => 'Ohio',
      'OK' => 'Oklahoma',
      'OR' => 'Oregon',
      'PA' => 'Pennsylvania',
      'RI' => 'Rhode Island',
      'SC' => 'South Carolina',
      'SD' => 'South Dakota',
      'TN' => 'Tennessee',
      'TX' => 'Texas',
      'UT' => 'Utah',
      'VT' => 'Vermont',
      'VA' => 'Virginia',
      'WA' => 'Washington',
      'WV' => 'West Virginia',
      'WI' => 'Wisconsin',
      'WY' => 'Wyoming',
      'DC' => 'District of Columbia',
      'AS' => 'American Samoa',
      'GU' => 'Guam',
      'MP' => 'Northern Mariana Islands',
      'PR' => 'Puerto Rico',
      'VI' => 'Virgin Islands, U.S.',
    );
    $childtype = array(
      'neighborhood' => t('Neighborhood'),
      'city' => t('City'),
      'zipcode' => t('Zipcode'),
    );

    $form['#attached']['library'] = array('zillow/jquery.geocomplete');

    $form['childtype'] = array(
      '#type' => 'select',
      '#title' => t('The type of subregions'),
      '#description' => t('Select type of subregions to retrieve.'),
      '#attributes' => array(
        'name' => array('childtype'),
      ),
      '#options' => $childtype,
    );

    $form['city'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Choose City:'),
      '#description' => t('Specify city location (Carries validity check)'),
      '#attributes' => array(
        'class' => array('citycomplete'), /* Set class for assigning to input geocoder autocomplete feature. */
      ),
    );

    $form['e_message'] = array(
      '#type' => 'markup',
      '#markup' => '<div class="tooltip">' . $this->t('Wrong city!') . '</div>',
    );

    $form['OR'] = array(
      '#type' => 'markup',
      '#markup' => '<span>' . $this->t('OR') . '</span>',
    );

    $form['state'] = array(
      '#type' => 'select',
      '#title' => t('U.S. State'),
      '#description' => t('Choose U.S. State to search regions.'),
      '#options' => $us_states,
      '#states' => array(
        'enabled' => array(
          ':input.citycomplete' => array('value' => ''),
        ),
      ),
    );

    $form['button'] = array(
      '#type' => 'button',
      '#value' => t('Search'),
      '#ajax' => array(
        'callback' => 'Drupal\zillow\Plugin\Form\ZillowRegionSearchBlockForm::validateAddressAjax',
        'event' => 'click',
        'progress' => array(
          'type' => 'throbber',
          'message' => $this->t('Checking address...'),
        ),
      ),
    );
    return $form;
  }

  /**
   * Address validation callback function.
   *
   * @param array $form
   *    Form.
   * @param FormStateInterface $form_state
   *    FormState.
   *
   * @return AjaxResponse
   *    AjaxResponse.
   */
  public function validateAddressAjax(array &$form, FormStateInterface $form_state) {
    // Instantiate an AjaxResponse Object to return.
    $ajax_response = new AjaxResponse();

    // Get input value.
    $search = $form_state->getValue('state') . ' ' . $form_state->getValue('city');

    // Initialize Geocoder.
    $gc = new GoogleGeocoder();
    $placemark = $gc->geocode($search);

    // Check for a valid Placemark and act accordingly.
    if ($placemark->isValid() === FALSE) {
      // Add a command, InvokeCommand, which allows for custom jQuery commands.
      $ajax_response->addCommand(new InvokeCommand(NULL, 'search_alert', array($form_state->getValue('city'))));
    }
    else {
      // If address is correct - add data to form. Also check if address is full.
      if (empty($placemark->city) && !empty($placemark->county)) {
        $city = preg_replace(array('/Country/', '/country/', '/County/'), array('', '', ''), $placemark->county);
      }
      elseif (!empty($placemark->city)) {
        $city = $placemark->city;
      }
      else {
        $city = '#';
      }
      $url = Url::fromRoute('zillow.region', array(
        'state' => Request::normalizeQueryString($placemark->state),
        'childtype' => $form_state->getValue('childtype'),
        'city' => $city,
      ), array('absolute' => TRUE));
      // Add a command, InvokeCommand, which allows for custom jQuery commands.
      // On validate - redirect to zillow result page using GET request.
      $ajax_response->addCommand(new RedirectCommand($url->toString()));
    }

    // Return the AjaxResponse Object.
    return $ajax_response;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
