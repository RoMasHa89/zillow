<?php
/**
 * @file
 * Contains \Drupal\zillow\ZillowSettingsForm
 */
namespace Drupal\zillow;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Configure hello settings for this site.
 */
class ZillowSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'zillow_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'zillow.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('zillow.settings');

    $options_chart = array(
      '1year' => '1',
      '5years' => '5',
      '10years' => '10',
    );
    $options_comp = array(
      1 => '1',
      2 => '2',
      3 => '3',
      4 => '4',
      5 => '5',
      6 => '6',
      7 => '7',
      8 => '8',
      9 => '9',
      10 => '10',
    );

    $options_results_limit = array(
      10 => '10',
      20 => '20',
      30 => '30',
      40 => '40',
      50 => '50',
      60 => '60',
      70 => '70',
      80 => '80',
      90 => '90',
      100 => '100',
    );

    $form['api_key'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Zillow API key'),
      '#description' => t('Type your personal Zillow API key. %link to get one.',
        array(
          '%link' => Link::fromTextAndUrl(
            $this->t('Click here'),
            Url::fromUri('http://www.zillow.com/webservice/Registration.htm')
          )->toString(),
        )
      ),
      '#default_value' => $config->get('api_key'),
    );

    $form['google_api_key'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Google API key'),
      '#description' => t('Type your Google API key to make available map preview miniature in Region results page. %link to get one.',
        array(
          '%link' => Link::fromTextAndUrl(
            $this->t('Click here'),
            Url::fromUri('https://developers.google.com/maps/documentation/javascript/get-api-key')
          )->toString(),
        )
      ),
      '#default_value' => $config->get('google_api_key'),
    );

    $form['region_results_limit'] = array(
      '#type' => 'select',
      '#title' => t('Results per page on Region Search Results Page'),
      '#description' => t('Choose how much items to show on Region Search Results Page'),
      '#options' => $options_results_limit,
      '#default_value' => $config->get('region_results_limit') ?: '50',
    );

    $form['chart_period'] = array(
      '#type' => 'select',
      '#title' => t('Chart Period'),
      '#description' => t('Choose chart period, years'),
      '#options' => $options_chart,
      '#default_value' => $config->get('chart_period') ?: '1year',
    );

    $form['comps_num'] = array(
      '#type' => 'select',
      '#title' => t('Comparables Number'),
      '#description' => t('Choose comparables number to search.'),
      '#options' => $options_comp,
      '#default_value' => $config->get('comps_num') ?: 5,
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('zillow.settings')
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('google_api_key', $form_state->getValue('google_api_key'))
      ->set('chart_period', $form_state->getValue('chart_period'))
      ->set('region_results_limit', $form_state->getValue('region_results_limit'))
      ->set('comps_num', $form_state->getValue('comps_num'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
