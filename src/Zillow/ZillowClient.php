<?php
/**
 * This file is part of the Zillow package.
 *
 * @file
 * Contains \Drupal\zillow\Zillow\ZillowClient
 *
 * @author Vincent Gabriel <vadimg88@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drupal\zillow\Zillow;

use GuzzleHttp\Client as GuzzleClient;
use Goutte\Client as GoutteClient;
use GuzzleHttp\ClientInterface as GuzzleClientInterface;
use Symfony\Component\Serializer\Encoder\XmlEncoder as BaseXmlEncoder;

/**
 * Client.
 *
 * @author Vincent Gabriel <vadimg88@gmail.com>
 */
class ZillowClient {

  /**
   * @var ZillowClient api endpoint
   */
  const END_POINT = 'http://www.zillow.com/webservice/';
  /**
   * @var object GuzzleClient
   */
  protected $client;

  /**
   * @var string ZWSID
   */
  protected $ZWSID;

  /**
   * @var int
   */
  protected $errorCode = 0;

  /**
   * @var string
   */
  protected $errorMessage = NULL;

  /**
   * @var array
   */
  protected $response;

  /**
   * @var array
   */
  protected $results;

  /**
   * @var array
   */
  protected $photos = [];

  /**
   * @var array - valid callbacks
   */
  public static $validCallbacks = [
    'GetZestimate',
    'GetSearchResults',
    'GetChart',
    'GetComps',
    'GetDeepComps',
    'GetDeepSearchResults',
    'GetUpdatedPropertyDetails',
    'GetDemographics',
    'GetRegionChildren',
    'GetRegionChart',
    'GetRateSummary',
    'GetMonthlyPayments',
    'CalculateMonthlyPaymentsAdvanced',
    'CalculateAffordability',
    'CalculateRefinance',
    'CalculateAdjustableMortgage',
    'CalculateMortgageTerms',
    'CalculateDiscountPoints',
    'CalculateBiWeeklyPayment',
    'CalculateNoCostVsTraditional',
    'CalculateTaxSavings',
    'CalculateFixedVsAdjustableRate',
    'CalculateInterstOnlyVsTraditional',
    'CalculateHELOC',
  ];

  /**
   * Initiate the class.
   *
   * @param string $ZWSID
   *    ZWS ID.
   */
  public function __construct($ZWSID) {
    $this->setZWSID($ZWSID);
  }

  /**
   * Set client.
   *
   * Return GuzzleClient.
   */
  public function setClient(GuzzleClientInterface $client) {
    $this->client = $client;

    return $this;
  }

  /**
   * Get GuzzleClient, create if it's null.
   *
   * Return GuzzleClient.
   */
  public function getClient() {
    if (!$this->client) {
      $this->client = new GuzzleClient(array('defaults' => array('allow_redirects' => FALSE, 'cookies' => TRUE)));
    }

    return $this->client;
  }

  /**
   * Set new ZWS ID.
   *
   * @return string
   *    ZWSID
   */
  public function setZWSID($id) {
    return ($this->ZWSID = $id);
  }

  /**
   * Get ZWS ID.
   *
   * @return string
   *    ZWSID
   */
  public function getZWSID() {
    return $this->ZWSID;
  }

  /**
   * Check if the last request was successful.
   *
   * @return bool
   *    Boolean.
   */
  public function isSuccessful() {
    return (bool) ((int) $this->errorCode === 0);
  }

  /**
   * Return the status code from the last call.
   *
   * @return int
   *    Status Code.
   */
  public function getStatusCode() {
    return $this->errorCode;
  }

  /**
   * Return the status message from the last call.
   *
   * @return string
   *    Status message.
   */
  public function getStatusMessage() {
    return $this->errorMessage;
  }

  /**
   * Return the actual response array from the last call.
   *
   * @return array
   *    Returns response.
   */
  public function getResponse() {
    return isset($this->response['response']) ? $this->response['response'] : $this->response;
  }

  /**
   * Return the results array from the GetSearchResults call.
   *
   * @return array
   *    Returns results array.
   */
  public function getResults() {
    return $this->results;
  }

  /**
   * Magic method to invoke the correct API call
   * if the passed name is within the valid callbacks.
   *
   * @param string $name
   *    Name.
   * @param array $arguments
   *    Arguments.
   *
   * @return array
   *    Request object.
   *
   * @throws ZillowException
   */
  public function __call($name, $arguments) {
    if (in_array($name, self::$validCallbacks)) {
      return $this->doRequest($name, $arguments);
    }
    else {
      throw new ZillowException("Wrong Callback!");
    }
  }

  /**
   * Since zillow does not provide the ability to grab the photos
   * of the properties through the API this little method will scan
   * the property url and grab all the images for that property.
   *
   * @param string $uri
   *    URL.
   *
   * @return array
   *    Array of HTML <img> objects.
   */
  public function GetPhotos($uri) {
    $this->photos = [];
    $client = new GoutteClient();
    $crawler = $client->request('GET', $uri);

    // Get the latest post in this category and display the titles.
    $crawler->filter('.photos a')->each(function ($node) {
      $this->photos[] = $node->filter('img')->attr('src') ?: $node->filter('img')->attr('href');
    });

    $this->response = $this->photos;

    return $this->response;
  }

  /**
   * Works the same way but instead passing a uri
   * you can pass a zpid and it will perform a request to grab the uri
   * based on the id.
   *
   * @see GetPhotos
   * @param int @zpid
   *    Zpid.
   *
   * @return array
   *    Response string.
   */
  public function GetPhotosById($zpid) {
    // We call the GetZestimate first to get the link to the home page details.
    $response = $this->GetZestimate(['zpid' => $zpid]);

    $this->photos = [];
    if ($this->isSuccessful() && isset($response['links']['homedetails']) && $response['links']['homedetails']) {
      return $this->GetPhotos($response['links']['homedetails']);
    }
    else {
      $this->setStatus(999, 'COULD NOT GET PHOTOS');
    }

    return $this->response;
  }

  /**
   * Perform the actual request to the zillow api endpoint.
   *
   * @param mixed $call
   *    Call.
   * @param array $params
   *    Params.
   *
   * @return array
   *    Array.
   *
   * @throws ZillowException
   */
  protected function doRequest($call, array $params) {
    // Validate.
    if (!$this->getZWSID()) {
      throw new ZillowException("You must submit the ZWSID");
    }

    // Run the call.
    $response = $this->getClient()->get(self::END_POINT . $call . '.htm', ['query' => array_merge(['zws-id' => $this->getZWSID()], $params[0])]);

    $xml = new BaseXmlEncoder();
    $this->response = $xml->decode($response->getBody()->getContents(), 'xml');
    unset($xml);
    // Parse response.
    return $this->parseResponse($this->response);
  }

  /**
   * Set the statis code and message of the api call.
   *
   * @param int $code
   *    Response Code.
   * @param string $message
   *    Message text.
   */
  protected function setStatus($code, $message) {
    $this->errorCode = $code;
    $this->errorMessage = $message;
  }

  /**
   * Parse the reponse into a formatted array.
   *
   * Also set the status code and status message.
   *
   * @param object $response
   *    Responce object.
   *
   * @return array
   *    Response array.
   */
  protected function parseResponse($response) {
    // Init.
    $this->response = json_decode(json_encode($response), TRUE);

    if (!$this->response['message']) {
      $this->setStatus(999, 'XML WAS NOT FOUND');
      return FALSE;
    }

    // Check if we have an error.
    $this->setStatus($this->response['message']['code'], $this->response['message']['text']);

    // If request was succesful then parse the result.
    if ($this->isSuccessful()) {
      if ($this->response['response'] && isset($this->response['response']['results']) && count($this->response['response']['results'])) {
        foreach ($this->response['response']['results'] as $result) {
          if (isset($result[0])) {
            /* Multiple results. */
            foreach ($result as $r) {
              $this->results[$r['zpid']] = $r;
            }
          }
          else {
            /* One result. */
            $this->results[$result['zpid']] = $result;
          }
        }
      }
    }

    return isset($this->response['response']) ? $this->response['response'] : $this->response;
  }

}
