<?php
/**
 * @file
 * Contains \Drupal\zillow\Zillow\ZillowException
 * This file is part of the Zillow package.
 *
 * @author Vincent Gabriel <vadimg88@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drupal\zillow\Zillow;

/**
 * Default exception class.
 */
class ZillowException extends \Exception {
}
