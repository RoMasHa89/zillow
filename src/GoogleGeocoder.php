<?php
/**
 * @file
 * Contains \Drupal\zillow\GoogleGeocoder
 *
 * @author Rob Apodaca <rob.apodaca@gmail.com>
 *
 * @copyright Copyright (c) 2009, Rob Apodaca
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Drupal\zillow;

use Drupal\zillow\GooglePlacemark;

/**
 * Class GoogleGeocoder.
 *
 * @package Drupal\zillow
 */
class GoogleGeocoder {
  private $addresses = array();

  public function __construct() {
  }

  /**
   * Geocodes (or attempts to) the supplied address. This method can be safely
   * called repeatedly without hitting the google geocode service. Each address
   * result is stored in this Google_Geocoder object and used whenever same
   * address is supplied to this method.
   *
   * @param string $address
   *    Address.
   *
   * @access public
   * @return Google_Placemark
   */
  public function geocode($address) {
    if (FALSE === array_key_exists($address, $this->addresses)) {
      $result = file_get_contents($this->getUri($address));
      $this->addresses[$address] = $result;
    }

    return new GooglePlacemark($this->addresses[$address]);
  }

  /**
   * Fetches the address from Google's geocode service.
   *
   * @param string $address
   *    Address.
   *
   * @access private
   *
   * @return string
   *    Result.
   */
  private function getUri($address) {
    $uri = "http://maps.googleapis.com/maps/api/geocode/json?";
    $uri .= "address=" . urlencode($address);
    $uri .= "&sensor=false";
    return $uri;
  }

}
